#include <iostream>

class Animal
{
public:
	virtual void Voice() = 0;
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Dog goes WOOF\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Cat goes MEOW\n";
	}
};

class Bird : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Bird goes TWEET\n";
	}
};

class Mouse : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Mouse goes SQUEEK\n";
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Cow goes MOO\n";
	}
};

class Frog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Frog goes CROAK\n";
	}
};

class Elephant : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Elephnt goes TOOT\n";
	}
};

class Duck : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Duck say QUACK\n";
	}
};

class Fish : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Fish go BLUB\n";
	}
};

class Seal : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Seal goes OW OW OW\n";
	}
};

class Fox : public Animal
{
public:
	void Voice() override
	{
		std::cout << "What does the Fox say?\n";
	}
};


int main()
{
	Animal* Animals[11];
	Animals[0] = new Dog();
	Animals[1] = new Cat();
	Animals[2] = new Bird();
	Animals[3] = new Mouse();
	Animals[4] = new Cow();
	Animals[5] = new Frog();
	Animals[6] = new Elephant();
	Animals[7] = new Duck();
	Animals[8] = new Fish();
	Animals[9] = new Seal();
	Animals[10] = new Fox();
	
	for (Animal* p : Animals)
		p->Voice();
}